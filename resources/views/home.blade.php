<!DOCTYPE html>
<html>
<head>
<style>
table {
  font-family: arial, sans-serif;
  border-collapse: collapse;
  width: 100%;
}

td, th {
  border: 1px solid #dddddd;
  text-align: left;
  padding: 8px;
}

tr:nth-child(even) {
  background-color: #dddddd;
}
</style>
</head>
<body>

<h2>Dealer Report</h2>

<table>
  <tr>
    <th>Dealer Name</th>
    <th>Total Leads</th>
    <th>Leads Value</th>
    <th>Revenue Generated</th>
    <th>Amount Lost</th>
  </tr>
  @foreach ($dealer as $dealers)
  <tr>
    <td>{{ $dealers->dealer_name }}</td>
    <?php $leads = DB::table('leads')->where('dealer_id','=',$dealers->id)->count();?>
    <td>{{ $leads }}</td>
    <?php $leads_value = DB::table('model_descriptions')->where('brand_code','=',$dealers->brand_code)->sum('rrp');?>
    <td>{{ $leads_value }}</td>
    <td>0</td>
    <td>0</td>
  </tr>
  @endforeach
</table>

</body>
</html>
